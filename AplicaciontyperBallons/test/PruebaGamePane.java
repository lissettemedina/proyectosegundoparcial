
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import modelo.Palabra;
import roots.GamePane;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author User
 */
public class PruebaGamePane extends Application {

    private GamePane gp;

    @Override
    public void start(Stage primaryStage) {
        gp = new GamePane();
        Scene sc = new Scene(gp.getRoot());
        primaryStage.setScene(sc);
        primaryStage.show();
    }

    public static void main(String[] args) {
        System.out.println(Palabra.abecedario.length/2);
    }

}
