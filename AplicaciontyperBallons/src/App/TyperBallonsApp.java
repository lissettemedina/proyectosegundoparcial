/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App;

import data.CONSTANTES;
import javafx.application.Application;
import static javafx.application.Application.launch;

import javafx.scene.Scene;
import javafx.stage.Stage;
import roots.MenuPane;

/**
 *
 * @author LissetteMedina
 */
public class TyperBallonsApp extends Application {
    private static MenuPane mp=new MenuPane();
    private static Scene theScene=new Scene(mp.getRoot(),700,700);

    public static MenuPane getMp() {
        return mp;
    }

    public static void setMp(MenuPane mp) {
        TyperBallonsApp.mp = mp;
    }

    public static Scene getTheScene() {
        return theScene;
    }

    public static void setTheScene(Scene theScene) {
        TyperBallonsApp.theScene = theScene;
    }
    
    public static void TerminarHilos(){
        if(mp.getGp()!=null){
            mp.getGp().setInicializador(false);
            mp.getGp().setTerminarJuego(false);
            if(mp.getGp().getRp()!=null && mp.getGp().getRp().getGp2()!=null) mp.getGp().getRp().getGp2().setTerminarJuego(true);
        }
    }
    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setScene(theScene);
        primaryStage.setWidth(CONSTANTES.GAME_WIDTH);
        primaryStage.setHeight(CONSTANTES.GAME_HEIGHT);
        primaryStage.setTitle("*TyperBallons*");
        primaryStage.show();
    }
    
    @Override
    public void stop(){
        TerminarHilos();
    }
    public static void main(String[] args) {
        launch(args);
    }
}
