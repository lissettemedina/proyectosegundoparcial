/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exceptions;

/**
 *
 * @author PedroFarinango
 */
public class ArchivoExcepcion extends Exception {

    final String archivo;

    public ArchivoExcepcion(String archivo) {
        this(archivo, "No se puede procesar el archivo" + archivo);
    }

    public ArchivoExcepcion(String archivo, String mensaje) {
        super(mensaje);
        this.archivo = archivo;
    }

    public String getFilename() {
        return archivo;
    }
}
