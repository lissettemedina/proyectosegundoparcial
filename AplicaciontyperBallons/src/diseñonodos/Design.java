/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diseñonodos;

import data.CONSTANTES;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

/**
 *
 * @author LissetteMedina
 */
public class Design {

    private Design() {
    }
    
    public static void buttonDesign(Button b, String size){
        b.setStyle("-fx-background: #eb94d0;"+
                "-fx-border-radius: 8px;" + "-fx-border-color: #0000cd;"
                + "-fx-border-width: 1.7px;" + "-fx-background-color: #1e90ff;"
                + CONSTANTES.tamano +size+ "em;" + CONSTANTES.ESTILO
                + CONSTANTES.letra + "-fx-text-fill:#ffffff"
                
                );
        
    }
    public static void labelDesign(Label l, String size){
        l.setStyle(CONSTANTES.ESTILO
                + CONSTANTES.letra + CONSTANTES.tamano+size+"em;");
    }
    
    public static void labelDesign(Label l, String size, String color){
        l.setStyle(CONSTANTES.ESTILO
                                                + CONSTANTES.letra + CONSTANTES.tamano +size+"em;"+ "-fx-text-fill:"+color);
    }
    
    public static void hboxDesign(HBox h){
        h.setStyle("-fx-background-color: #e3f4f7;"
                + "-fx-background-radius: 10.0;"
                + "-fx-border-color: #29bedc;");
    }
    
}
