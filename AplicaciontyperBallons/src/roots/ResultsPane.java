/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package roots;

import App.TyperBallonsApp;
import data.CONSTANTES;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import modelo.Palabra;
import diseñonodos.Design;
/**
 *
 * @author LissetteMedina
 */
public class ResultsPane {

    private final StackPane root;
    private final VBox opciones;
    private HBox seccionResultados;
    private GamePane2 gp2;
    /**
     * Constructor que muestra en pantalla los nodos respectivos, boton y grid 
     * con los resultados de las letras escogidas por el usuario.
     */
    public ResultsPane() {
        root = new StackPane();
        opciones = new VBox();
        crearFondo();
        root.getChildren().add(opciones);
        opciones.setSpacing(5);
        opciones.setAlignment(Pos.CENTER);
        opciones.getChildren().addAll(seccionTop(), seccionLabel(), seccionGrid());
        
    }   
    /**
     * Método que modifica el diseño del root principal y se implementa el fondo
     * con una imagen. 
     */
    public void crearFondo() {
        root.setPrefSize(CONSTANTES.GAME_WIDTH, CONSTANTES.GAME_HEIGHT);
        root.setStyle("-fx-background-image: url('" + CONSTANTES.PATH_IMAGES
                + "fondo2.png');"
                + "-fx-background-repeat: stretch;"
                + "-fx-background-size: " + CONSTANTES.GAME_WIDTH + " "
                + CONSTANTES.GAME_HEIGHT + "; "
                + "-fx-background-position: center center;");
    }
    /**
     * Método que crea un Botón llamado Continuar y que se le pasa un evento
     * que permita cambiar el root de la escena.
     * @return Devuelve el botón creado para posteriormente agregarlo al root 
     * principal.
     */
    public Button seccionTop() {
        Button bContinuar = new Button("Continuar");
       Design.buttonDesign(bContinuar, "2.5");
        bContinuar.setOnMouseClicked((e)->{
            gp2= new GamePane2();
            TyperBallonsApp.getTheScene().setRoot(gp2.getRoot());
        });
        return bContinuar;

    }
    /**
     * Método que crea un Label llamado Resultados.
     * @return Devuelve el label creado, con su respectivo diseño.
     */
    public Label seccionLabel() {
        Label l = new Label("Resultados");
        Design.labelDesign(l, "2.5");
        return l;

    }
    /**
     * Método que crea un GridPane donde se almacenará las letras escogidas por
     * el usuario con su determinada cantidad de veces de acuerdo a los globos
     * reventados.
     * @return Devuelve un GridPane.
     */
    public HBox seccionGrid() {
        seccionResultados = new HBox(50);
        seccionResultados.setAlignment(Pos.CENTER);
        letrasEnPantalla(seccionResultados);
        return seccionResultados;
    }
    /**
     * Método que llena al GridPane con labels donde se almacenará la letra y 
     * la cantidad de letras escogidas por el jugador. 
     * @param seccionLetras,  GridPane al cual se le agregará los Labels con 
     * la información respectiva.
     */
    public void letrasEnPantalla(HBox seccionLetras){
        int i = 0;
        seccionLetras.getChildren().clear();
        if (GamePane.letrasMap.keySet().size()<(Palabra.abecedario.length/2)){
            GridPane stockLetras = new GridPane();
            for (String clave:GamePane.letrasMap.keySet()) {
                Label lLetra = new Label(clave);
                Design.labelDesign(lLetra, "2.2");
                if(GamePane.letrasMap.get(clave)!=0){
                    Label lPuntos = new Label(String.valueOf(GamePane.letrasMap.get(clave)));
                    Design.labelDesign(lPuntos, "2.2");
                    GridPane.setVgrow(lLetra, Priority.ALWAYS);
                    stockLetras.addRow(i, lLetra, lPuntos);
                    i++; 
                } 
            }
            stockLetras.setVgap(10); stockLetras.setHgap(30); seccionLetras.getChildren().add(stockLetras);
        }else{
            GridPane stockLetras1= new GridPane();
            GridPane stockLetras2 = new GridPane();
            for (String clave:GamePane.letrasMap.keySet()) {
                
                if (i<(GamePane.letrasMap.keySet().size()/2)){
                    Label lLetra = new Label(clave);
                    Design.labelDesign(lLetra, "2.2");
                    if(GamePane.letrasMap.get(clave)!=0){
                        Label lPuntos = new Label(String.valueOf(GamePane.letrasMap.get(clave)));
                        Design.labelDesign(lPuntos, "2.2");
                        stockLetras1.addRow(i, lLetra, lPuntos);
                        i++; 
                    } 
                }else{
                    Label lLetra = new Label(clave);
                    Design.labelDesign(lLetra, "2.2");
                    if(GamePane.letrasMap.get(clave)!=0){
                        Label lPuntos = new Label(String.valueOf(GamePane.letrasMap.get(clave)));
                        Design.labelDesign(lPuntos, "2.2");
                        stockLetras2.addRow(i-(int)GamePane.letrasMap.keySet().size()/2, lLetra, lPuntos);
                        i++; 
                    }
                }
            
            }
            stockLetras1.setVgap(10); stockLetras1.setHgap(30); stockLetras2.setVgap(15); stockLetras2.setHgap(30);
            seccionLetras.getChildren().addAll(stockLetras1, stockLetras2); seccionLetras.setPadding(new Insets(3,40,40,40));
        }     
        seccionLetras.setMaxSize(280,CONSTANTES.GAME_HEIGHT-100);
    }
    
    public HBox getSeccionResultados() {
        return seccionResultados;
    }

    public GamePane2 getGp2() {
        return gp2;
    }
    
    public StackPane getRoot() {
        return root;
    }
    
}
