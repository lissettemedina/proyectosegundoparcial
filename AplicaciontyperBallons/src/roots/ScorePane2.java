/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package roots;

import App.TyperBallonsApp;
import data.CONSTANTES;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import modelo.Score;
import diseñonodos.Design;

/**
 *
 * @author LissetteMedina
 */
public class ScorePane2 {

    private final Pane root;
    private final VBox opciones;
    public static int user=1;
    
/**
 * Constructor donde se muestra la pantalla de la información con los 10 mejores
 * resultados. 
 */
    public ScorePane2() {
        root = new Pane();
        crearFondo();
        opciones = new VBox();
        opciones.setSpacing(20);
        opciones.setAlignment(Pos.CENTER);
        opciones.setPadding(new Insets(40,1, 0, 140));
        if(seccionScore()!=null){
            opciones.getChildren().addAll(seccionBox(),seccionScore());
        }else{
            opciones.getChildren().add(seccionBox());
        }
        root.getChildren().addAll(botonMenu(),opciones);
    }
    /**
     * Método que modifica el diseño del root principal.
     */
    public void crearFondo() {
        root.setPrefSize(CONSTANTES.GAME_WIDTH, CONSTANTES.GAME_HEIGHT);
        root.setStyle("-fx-background-image: url('" + CONSTANTES.PATH_IMAGES
                + "fondoScores.jpg');"
                + "-fx-background-repeat: stretch;"
                + "-fx-background-size: " + CONSTANTES.GAME_WIDTH + " "
                + CONSTANTES.GAME_HEIGHT + "; "
                + "-fx-background-position: center center;");
    }
    
    public Pane getRoot() {
        return root;
    }
    /**
     * Método que crea un Botón y se le implementa un ícono asociado al mismo. 
     * Se le pasa un evento que permite regresar al menú principal.
     * @return Botón de regreso al Menú principal.
     */
    public Button botonMenu(){
        try {
            Button botonMenu= new Button();
            Image img = new Image(new FileInputStream("src" + CONSTANTES.PATH_IMAGES + "iconoMenu.png"));
            ImageView imgV = new ImageView(img);
            imgV.setFitHeight(65);
            imgV.setFitWidth(75);
            botonMenu.setGraphic(imgV);
            botonMenu.setBackground(Background.EMPTY);
            botonMenu.setLayoutX(602);
            botonMenu.setLayoutY(592);
            
            botonMenu.setOnMouseClicked((e)->{
                TyperBallonsApp.setMp(new MenuPane());
                TyperBallonsApp.getTheScene().setRoot(TyperBallonsApp.getMp().getRoot());
            });
            return botonMenu;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ScorePane2.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    /**
     * Método que crea un HBox donde se almacenará un label con el nombre de 
     * "Score" y una imagen de decoración.
     * @return Hbox con la información.
     */
    public HBox seccionBox(){
        try {
            HBox score = new HBox();
            Image img = new Image(new FileInputStream("src" + CONSTANTES.PATH_IMAGES + "tituloScore.png"));
            ImageView imgV = new ImageView(img);
            imgV.setFitHeight(70);
            imgV.setFitWidth(320);
            Image img2 = new Image(new FileInputStream("src" + CONSTANTES.PATH_IMAGES + "iconoCopa.png"));
            ImageView imgV2 = new ImageView(img2);
            imgV2.setFitHeight(70);
            imgV2.setFitWidth(85);
            score.getChildren().addAll(imgV,imgV2);
            return score;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ScorePane2.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    /**
     * Método que crea un GridPane con los 10 mejores resultados. 
     * Se itera sobre un Set, proporcionado por el método leerArchivo, que
     * almacena la información de los 10 mejores scores.
     * En caso de lanzarse una excepción, se mostrará en pantalla mediante un label.
     * @return GridPane con la información de los 10 mejores scores.
     */
    public GridPane seccionScore(){
        try {
            GridPane gp= new GridPane();
            gp.setStyle("-fx-background-color: #DCE9EE;"
                + "-fx-background-radius: 10.0;"
                + "-fx-border-color: #AFB0B1;");
            gp.setMaxSize(270,CONSTANTES.GAME_WIDTH-100);
            gp.setHgap(70);
            gp.setVgap(9);
            Label nombre=new Label("Nombre");
            Label puntaje=new Label("puntaje");
            Design.labelDesign(nombre, "2", "#FB830B");
            Design.labelDesign(puntaje, "2", "#FB830B");
            gp.addRow(0, nombre, puntaje);
            Set<Score> set= Score.leerArchivo();
            int i=1;
            Iterator<Score> iter= set.iterator();
           while (iter.hasNext()&& i<11){
               Score score=iter.next();
               Label nombreU= new Label(score.getNombre());
               Design.labelDesign(nombreU, "2");
               Label puntajeU = new Label(String.valueOf(score.getPuntaje()));
               Design.labelDesign(puntajeU, "2");
               gp.addRow(i, nombreU, puntajeU);
               i++;
           }
            return gp;
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(ScorePane2.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
