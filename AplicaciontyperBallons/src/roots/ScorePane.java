/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package roots;

import App.TyperBallonsApp;
import data.CONSTANTES;
import java.awt.Desktop;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import modelo.Score;
import diseñonodos.Design;

/**
 *
 * @author LissetteMedina
 */
public  class ScorePane {
    StackPane root;
    VBox opcionesBox;
    TextField tf;
    /**
     * Constructor que muestra en pantalla el Score alcanzado por el usuario 
     * y donde pide su nombre.
     */
    public ScorePane(){
        
        try {
            root = new StackPane();
            opcionesBox = new VBox();
            opcionesBox.setSpacing(20);
            opcionesBox.setAlignment(Pos.CENTER);
            crearFondo();
            opcionesBox.setMaxSize(450, 350);
            opcionesBox.setStyle("-fx-background-color: rgba(255, 255, 255, 0.5);"+
                    "-fx-border-radius: 10px;");
            opcionesBox.getChildren().addAll(seccionPuntaje(),seccionBoton(),seccionUsuario(),botonOK());
           
            root.getChildren().add(opcionesBox);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ScorePane.class.getName()).log(Level.SEVERE, null, ex);
        }
       
    }
     public StackPane getRoot(){
        return root;
    }
     /**
      * Método que modifica el diseño del root principal.
      */
    public void crearFondo() {
        root.setPrefSize(CONSTANTES.GAME_WIDTH, CONSTANTES.GAME_HEIGHT);
        root.setStyle("-fx-background-image: url('" + CONSTANTES.PATH_IMAGES + "fondoO.jpg');"
                + "-fx-background-repeat: stretch;"
                + "-fx-background-size: " + CONSTANTES.GAME_WIDTH + " " + CONSTANTES.GAME_HEIGHT + "; "
                + "-fx-background-position: center center;");
    }
     /**
      * Método que Crea el botón para compartir el score alcanzado por el 
      * jugador.
      * @return Devuelve un botón de compartir en Twitter para agregarlo al 
      * root determinado.
      * @throws FileNotFoundException Arroja este error en caso de no encontrar
      * el archivo de imagen.
      */
    public Button seccionBoton() throws FileNotFoundException{
        Button btnCompartir = new Button();
        Image img = new Image(new FileInputStream("src" + CONSTANTES.PATH_IMAGES + "botonTwittear.jpg"));
        ImageView imgV = new ImageView(img);
        imgV.setFitHeight(30);
        imgV.setFitWidth(70);
        btnCompartir.setBackground(Background.EMPTY);
        btnCompartir.setGraphic(imgV);
        btnCompartir.setOnMouseClicked((e)->{
            
            try {    
                Desktop.getDesktop().browse(new URI ("https://twitter.com/intent/tweet?text"
                        + "=Tu%20score%20alcanzado%20es%20de:%20"+TyperBallonsApp.getMp().getGp().getRp().getGp2().getScore()+"&via=BallonsTyper"));
            } catch (URISyntaxException ex) {
                Logger.getLogger(ScorePane.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ScorePane.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        });
        return btnCompartir;
    }
    /**
     * Método que crea un HBox donde se almacenará la información del score en 
     * un imagen (StackPane) y un label con el mensaje de su puntaje.
     * @return Devuelve un HBox con la información.
     */
    public HBox seccionPuntaje(){
        HBox scoreBox= new HBox();
        scoreBox.setPadding(new Insets(0, 30, 0, 30));
        scoreBox.setSpacing(75);
        Label l= new Label ("Su puntaje es de: ");
        Design.labelDesign(l, "2.1","#f88c12");
        StackPane globo= new StackPane();
        globo.setPrefSize(75, 150);
        globo.setStyle("-fx-background-image: url('" + CONSTANTES.PATH_IMAGES + "fondoPuntaje2.png');"
                + "-fx-background-repeat: stretch;"
                + "-fx-background-size: " + 75 + " " + 150 + "; "
                + "-fx-background-position: center center;");

        Label puntaje = new Label (String.valueOf(TyperBallonsApp.getMp().getGp().getRp().getGp2().getScore()));
        Design.labelDesign(puntaje, "1.9","#ffffff");
        globo.getChildren().add(puntaje);
        StackPane.setAlignment(puntaje,Pos.CENTER);
        scoreBox.getChildren().addAll(l,globo);
        return scoreBox;
        }
    /**
     * Método que crea un HBox donde se almacena el label "Ingrese su nombre" y
     * un TextFied para que el usuario ingrese su nombre.
     * @return HBox con sus respectivos nodos.
     */
    public HBox seccionUsuario(){
        HBox usuarioBox = new HBox();
        usuarioBox.setPadding(new Insets(0, 30, 0, 30));
        usuarioBox.setSpacing(20);
        Label ingreso= new Label("Ingrese su nombre: ");
        Design.labelDesign(ingreso, "1.5","#fed821");
        tf= new TextField();
        tf.setMaxSize(170, 45);
        tf.setStyle(
                "-fx-font-size: 1.6em;"
                + "  -fx-font-family: \"Eras Bold ITC\", Georgia, Sans Serif;"
                + "  -fx-highlight-fill: #00ff00;"
                + "  -fx-background-color: #ffffff;"
                + "-fx-border-color: rgba(229,0,0,0.3)");
        usuarioBox.getChildren().addAll(ingreso,tf);
        return usuarioBox;
    }
    /**
     * Método que crea un Botón de "OK" y que se le pasa un evento donde se 
     * realiza la serializacion y deseralización del Objeto determinado con el
     * objetivo de almacenar la informacion de los scores alcanzados, es decir, 
     * nombre y puntaje asociado, en forma ordenada. 
     * @return Devuelve boton OK.
     */
    public Button botonOK(){
        Button b= new Button("OK");
        b.setStyle("-fx-background: #00aae4;"+
                "-fx-border-radius: 8px;" + "-fx-border-color: #00aae4;"
                + "-fx-border-width: 1.2px;" + "-fx-background-color: #00aae4;"
                + "-fx-font-size: 1.2em;" + "-fx-font-family: \"Eras Bold ITC\", "
                + "Georgia, Sans Serif;" + "-fx-text-fill:#ffffff"
                
                );
        b.setOnMouseClicked((e)->{
            String nombre=tf.getText().trim().toLowerCase();
            Score s= new Score(TyperBallonsApp.getMp().getGp().getRp().getGp2().getScore(),nombre);
            if(nombre.equals("")){
                s= new Score(TyperBallonsApp.getMp().getGp().getRp().getGp2().getScore(),"user");
            }
            
            TyperBallonsApp.setMp(new MenuPane());
            TyperBallonsApp.getTheScene().setRoot(TyperBallonsApp.getMp().getRoot());
            try {
                TreeSet<Score> scores = Score.leerArchivo();
                if(scores!= null){
                    s.escribirArchivo(scores);
                }
            } catch (IOException ex) {
                try {
                    s.escribirArchivo();
                } catch (IOException ex1) {
                    Logger.getLogger(ScorePane.class.getName()).log(Level.SEVERE, null, ex1);
                }
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ScorePane.class.getName()).log(Level.SEVERE, null, ex);
            } 
        });
        return b;
        
    }
}

