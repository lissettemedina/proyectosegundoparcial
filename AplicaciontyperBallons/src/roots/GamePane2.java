/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package roots;

import App.TyperBallonsApp;
import data.CONSTANTES;
import data.DiccionarioData;
import diseñonodos.Design;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import static javafx.scene.input.KeyCode.ENTER;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javax.sound.sampled.Clip;
import modelo.Palabra;
import sonido.CargarRecurso;

/**
 * Esta clase crea la pantalla de juego donde el jugador formará palabras.
 * @author PedroFarinango
 * @author Luis Sánchez
 * @author Lissette Medina
 */
public class GamePane2 {
    private HBox gamesPane2;
    private VBox top;
    private final StackPane root;
    private final VBox ingreso;
    private final VBox grid;
    private HBox results;
    private int tiempoTranscurrido = 30;
    private boolean terminarJuego= false;
    private Label tiempoLabel;
    private Label scoreLabel;
    private int score = 0;
    private ScorePane sc;
    private Clip sonido;

    /**
     *
     */
    public GamePane2() {
        root= new StackPane();
        ingreso = new VBox();
        grid = new VBox();
        root.getChildren().add(seccionCentro());
        StackPane.setAlignment(gamesPane2, Pos.CENTER_LEFT);
        ingreso.setAlignment(Pos.CENTER);
        grid.setAlignment(Pos.TOP_LEFT);
        grid.setPadding(new Insets(100, 0, 0, 30));
        gamesPane2.getChildren().addAll(grid, seccionIngreso(),crearTop());
        seccionGrid();
    }
    
    /**
     *Setea el estado del juego (false si el juego está activo, true en el caso contrario).
     * @param terminarJuego Boolean que indica si el juego ha concluido o no.
     */
    public void setTerminarJuego(boolean terminarJuego) {
        this.terminarJuego = terminarJuego;
    }
    
    /**
     * Crea la parte superior de la pantalla de juego, donde se encontrará el
     * cronometro y el score.
     * @return HBox que contendrá al cronometro y al score.
     */
    public VBox crearTop(){
        sonido= CargarRecurso.cargarSonido(CONSTANTES.PATH_AUDIO+"musicajuego2.wav");
        sonido.start();
        top= new VBox();
        Label lt = new Label("Tiempo: ");
        Design.labelDesign(lt, "1.6");
        Label lm = new Label("Score: ");
        Design.labelDesign(lm, "1.6");
        tiempoLabel = new Label(" 00:30");
        Design.labelDesign(tiempoLabel, "1.6");
        scoreLabel = new Label(String.valueOf(score));
        Design.labelDesign(scoreLabel, "1.6");
        HBox left= new HBox();
        HBox right= new HBox();
        left.getChildren().addAll(lt,tiempoLabel);
        right.getChildren().addAll(lm, scoreLabel);
        top.getChildren().addAll(left,right);
        top.setSpacing(5);
        top.setStyle("-fx-padding: 10;");
        return top;
    }
    
    /**
     * Metodo get del root de la clase GamePane2.
     * @return Devuelve el contenedor raíz de la clase.
     */
    public StackPane getRoot() {
        return root;
    }
    
/**
 * Crea la parte central, la cual contendrá el stock de letras y el área de ingreso
 * de palabras para el jugador.
 * @return StackPane que contendrá lo antes mencionado.
 */
    public HBox seccionCentro() {

        gamesPane2 = new HBox();
        gamesPane2.setPrefSize(CONSTANTES.GAME_WIDTH, CONSTANTES.GAME_HEIGHT);
        gamesPane2.setStyle("-fx-background-image: url('" + "/recursos/" + "fondo.jpeg');"
                + "-fx-background-repeat: stretch;"
                + "-fx-background-size: " + 700 + " "
                + 700 + "; "
                + "-fx-background-position: center center;");
        gamesPane2.setPadding(new Insets(20));
        new Thread(new HiloTiempo()).start();
        gamesPane2.setSpacing(7);
        return gamesPane2;
    }
/**
 * Crea la zona donde el jugador ingresará la palabra, validando la misma mediante 
 * un evento de tipo KeyEvent el cual después de verificarla, actualizará el 
 * stock de letras y en caso de no cumplir con los requerimientos nos mostrará 
 * un mensaje de palabra inválida el cual será borrado mediante un hilo.
 * @return VBox que contendrá toda la zona de ingreso.
 */
    public synchronized VBox seccionIngreso() {
        
        Label l = new Label("INGRESE PALABRA: ");
        Design.labelDesign(l, "1.5");
        TextField tf = new TextField();
        tf.setStyle(
                "-fx-font-size: 1.6em;"
                + "  -fx-font-family: \"Eras Bold ITC\", Georgia, Sans Serif;"
                + "  -fx-highlight-fill: #00ff00;"
                + "  -fx-background-color: #ffffff;"
                + "-fx-border-color: rgba(229,0,0,0.3)");
        tf.setMaxSize(200, 45);
        VBox zonamensaje = new VBox();
        ingreso.setSpacing(20);
        zonamensaje.setAlignment(Pos.CENTER);
        ingreso.getChildren().addAll(l, tf, zonamensaje);
        
        
        TyperBallonsApp.getTheScene().setOnKeyPressed((KeyEvent event) -> {
            String user=tf.getText().trim();
            switch(event.getCode()){
                case ENTER:
                    if(user!= null || Palabra.validarStringVacio(user)){ 
                        if (comprobarPalabra(user) && DiccionarioData.validarPalabra(user)){
                            String [] letrasIngresadas = user.toUpperCase().split("");
                            for (String letra : letrasIngresadas){
                                if (GamePane.letrasMap.containsKey(letra)){
                                    GamePane.letrasMap.replace(letra,
                                            GamePane.letrasMap.get(letra),
                                            GamePane.letrasMap.get(letra)-1);
                                    tf.clear();
                                    zonamensaje.getChildren().clear();
                                    score+=Palabra.letraPuntaje(letra);
                                    scoreLabel.setText(String.valueOf(score));
                                    seccionGrid();
                                }
                            }
                        }else{
                            String mensaje = "Palabra Invalida.";
                            Label lmensaje = new Label(mensaje);
                            zonamensaje.getChildren().clear();
                            zonamensaje.getChildren().add(lmensaje);
                            new Thread(()->{
                                try {
                                    Thread.sleep(2500);
                                } catch (InterruptedException ex) {
                                    Thread.currentThread().interrupt();
                                    Logger.getLogger(GamePane2.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                Platform.runLater(()->{
                                    zonamensaje.getChildren().clear(); 
                                });
                            }).start();
                        }
                            
                    }
                    break;
                default:
                    break;
            }
        });
      return ingreso;          
    }
/**
 * Crea el stock de letras en pantalla. 
 */
    public void seccionGrid() {
        grid.getChildren().clear();
        HBox nombre = new HBox(50);
        Design.hboxDesign(nombre);
        nombre.setMaxSize(260, CONSTANTES.GAME_HEIGHT - 100);
        results = TyperBallonsApp.getMp().getGp().getRp().seccionGrid();
        Design.hboxDesign(results);
        Label titulogrid1 = new Label("   Letra : Stock");
        Design.labelDesign(titulogrid1, "2");
        results.setPadding(new Insets(10, 12, 10, 12));
        nombre.getChildren().add(titulogrid1);
        StackPane.setAlignment(grid, Pos.TOP_LEFT);
        grid.setAlignment(Pos.CENTER);
        grid.setPadding(new Insets(20,3,50,10));
        grid.getChildren().addAll(nombre, results);
    }
    
    
    /**
     * Verifica que la palabra ingresada por el jugador tenga todas sus letras 
     * en stock.
     * @param palabraIngresada String que ingresa el jugador.
     * @return Boolean que nos indica si la palabra tiene todas sus letras en stock 
     * (true en caso de ser así, false en caso contrario).
     */
    public boolean comprobarPalabra(String palabraIngresada){
        boolean bandera = true;
        TreeMap<String, Integer> mapaPalabra = new TreeMap<>();
        String [] arPalabraIngresada = palabraIngresada.toUpperCase().split("");
        for (String letra : arPalabraIngresada){
            mapaPalabra.put(letra, mapaPalabra.getOrDefault(letra, 0)+1);
        }
        for (Map.Entry<String, Integer> obj: mapaPalabra.entrySet()){
            String clave=obj.getKey();
            if (GamePane.letrasMap.keySet().contains(clave)){
                if(!(mapaPalabra.get(clave)<=GamePane.letrasMap.get(clave))){
                    return false;
                }
            }else{
                return false;
            }    
        }
        return bandera;
    }
    /**
     * Una inner class que actualiza el cronometro pasando un segundo 
     * (El jugador solo tiene 30 segundos para formar tantas palabras como pueda
     * con las letras que tenga en stock).
     */
    private class HiloTiempo implements Runnable {

        @Override
        public void run() {
            while (!terminarJuego) {
                Platform.runLater(() -> {
                    tiempoTranscurrido -= 1;
                    if(tiempoTranscurrido>=10 && tiempoTranscurrido<60){    
                        tiempoLabel.setText(" 00:"+String.valueOf(tiempoTranscurrido));
                        Design.labelDesign(tiempoLabel, "1.6");
                    }else{
                        tiempoLabel.setText(" 00:0"+String.valueOf(tiempoTranscurrido));
                        Design.labelDesign(tiempoLabel,"1.6", "red");
                    }
                    if (tiempoTranscurrido == 0) {
                        terminarJuego = true;
                        sonido.stop();
                        sc = new ScorePane();
                        TyperBallonsApp.getTheScene().setRoot(sc.getRoot());
                    }
                });
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                    Logger.getLogger(GamePane2.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
        }
    }
    
    /**
     *Retorna el score obtenido por el jugador al finalizar la etapa de formar
     * palabras.
     * @return Un int con el score del jugador.
     */
    public int getScore(){
        return score;
    }
    
}
