/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package roots;

import App.TyperBallonsApp;
import data.CONSTANTES;
import diseñonodos.Design;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javax.sound.sampled.Clip;
import modelo.Globo;
import modelo.GloboA;
import modelo.GloboG;
import modelo.GloboR;
import sonido.CargarRecurso;

/**
 *
 * @author User
 */
public final class GamePane {

    private final BorderPane root;
    private Pane gamesPane;
    private boolean inicializador=true;
    private boolean terminarJuego = true;
    private Label cantidadLabel;
    private final Random random = new Random();
    private Label tiempoLabel;
    private static final long frecuenciaGlobo=300;
    private int cantidadGlobos;
    private int tiempoTranscurrido = 60;
    public final static  Map<String,Integer> letrasMap=new TreeMap<>();
    private ResultsPane rp;
    private Clip sonido;

    public GamePane(){
        root = new BorderPane();
        root.setCenter(crearCentro());
        root.setTop(crearTop());
    }
    
    /**
     * 
     * @return Devuelve el contenedor BorderPane.
     */
    public BorderPane getRoot() {
        return root;
    }

    public void setInicializador(boolean inicializador) {
        this.inicializador = inicializador;
    }

    public void setTerminarJuego(boolean terminarJuego) {
        this.terminarJuego = terminarJuego;
    }
    
    

    public ResultsPane getRp() {
        return rp;
    }
 
    /**
     * Se setea el size y el style del Pane.
     * @return Devuelve el Pane que estará en el centro del BorderPane(root).
     */
    public Pane crearCentro() {
        gamesPane = new Pane();
        gamesPane.setPrefSize(CONSTANTES.GAME_WIDTH, CONSTANTES.GAME_HEIGHT);
        gamesPane.setStyle("-fx-background-image: url('" + CONSTANTES.PATH_IMAGES
                + "fondo.jpeg');"
                + "-fx-background-repeat: stretch;"
                + "-fx-background-size: " + CONSTANTES.GAME_WIDTH + " "
                + CONSTANTES.GAME_HEIGHT + "; "
                + "-fx-background-position: center center;");
        new Thread(new HiloGlobo()).start();
        return gamesPane;
    }
   
    /**
     * Se coloca labels del tiempo transcurrido y globos reventados.
     * @return Devuelve un HBox que estará en la parte superior de BorderPane(root).
     */
    public HBox crearTop(){

        HBox top= new HBox();
        Label lt = new Label("Tiempo: ");
        Design.labelDesign(lt, "2.5");
        Label lm = new Label("Cantidad de globos: ");
        Design.labelDesign(lm, "2.5");
        tiempoLabel = new Label(" 01:00");
        Design.labelDesign(tiempoLabel, "2.5");
        cantidadLabel = new Label(String.valueOf(cantidadGlobos));
        Design.labelDesign(cantidadLabel, "2.5");
        HBox left= new HBox();
        HBox right= new HBox();
        left.setSpacing(2);
        left.getChildren().addAll(lt,tiempoLabel);
        right.setSpacing(2);
        right.getChildren().addAll(lm,cantidadLabel);
        top.getChildren().addAll(left,right);
        top.setSpacing(60);
        top.setStyle("-fx-padding: 10;" + "-fx-border-style: solid inside;"
        + "-fx-border-width: 2;" + "-fx-border-insets: 5;"
        + "-fx-border-radius: 5;" + "-fx-border-color: #8CDFF9;");
        top.setBackground(new Background(new BackgroundFill(Color.rgb(140, 223, 250), CornerRadii.EMPTY, Insets.EMPTY)));
        new Thread(new HiloInicio()).start();
        new Thread(new HiloTiempo()).start();
        return top;
    }
    
    /**
     * Se genera posiciones aleatorias para los globos en X y Y.
     * @return Devuelve un objeto de tipo Point2D con la posicion de globos de manera aleatoria.
     */
    private Point2D generarPosicion() {
        double x = random.nextInt((int) CONSTANTES.GAME_WIDTH - 92);
        double y = random.nextInt((int) (CONSTANTES.GAME_HEIGHT));
        return new Point2D(x, y);
    }
    
    /**
     * Se agrega en el mapa "letrasMap" las letras y su respectiva cantidad.
     * @param letra, La letra que se agregará en el mapa con su respectiva actualizacion del stock. 
     */
    private void agregarLetraGlobo(String letra){
        if(letrasMap.containsKey(letra)){
            letrasMap.put(letra, letrasMap.get(letra)+1);
        }else{
            letrasMap.put(letra,1);
        }
    }
    
    /**
     * De la clase "CONSTANTES" se genera de forma aleatoria un string ("v","a","r") que representa el color de los globos
     * @return Devuelte un string de las iniciales del color del globo.
     */
    public String tipoGlobo() {
        return CONSTANTES.TIPO_GLOBOS[random.nextInt(CONSTANTES.TIPO_GLOBOS.length)];
    }
    
    /**
     * Se genera un referencia de los 3 tipos de globos de manera aleatoria.
     * @return Devuelve un objeto que herede de globo.
     */
    public Globo generarReferenciaGlobo() {
        
        String[] tipos = CONSTANTES.TIPO_GLOBOS;
        String tipo = tipoGlobo();
        if (tipo.equals(tipos[0])) {
            return new GloboG();
        } else if (tipo.equals(tipos[1])) {
            return new GloboA();
        } else {
            return new GloboR();
        }
    }
    
    /**
     * Una inner class de un hilo que hará que la posición de un globo se modifique cada cierta frecuencia con la finalidad de que suba dicho globo.
     */
    class GloboMueve implements Runnable {
        @Override
        public void run() {
            Globo globo = generarReferenciaGlobo();
            Platform.runLater(() -> {
                gamesPane.getChildren().add(globo);});

            Point2D p = generarPosicion();
            globo.fijarPosicion(p.getX(), CONSTANTES.GAME_HEIGHT);
            while (terminarJuego) {
                Platform.runLater(() -> {
                    globo.cambiarPosicionY();});
                try {
                    Thread.sleep((long) globo.getFrecuencia());
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                    Logger.getLogger(GamePane.class.getName()).log(Level.SEVERE, null, ex);
                }
                if (globo.getLayoutY() <= -130) {
                    Platform.runLater(() -> {
                        gamesPane.getChildren().remove(globo);});
                    break;
                }
            }
        }
    }
    
    /**
     * Una inner class de un hilo que permite que se creen globos cada cierto intervalo de tiempo.
     */
    class HiloGlobo implements Runnable {
        @Override
        @SuppressWarnings("empty-statement")
        public void run() {
            while (inicializador) {
                try {
                    Thread.sleep(10);
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                    Logger.getLogger(GamePane.class.getName()).log(Level.SEVERE, null, ex);
                }};
            
            while (terminarJuego) {
               Thread th=new Thread(new GloboMueve());
                th.start();
                BorrarLabel();
                try {
                    Thread.sleep(frecuenciaGlobo*3);
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                    Logger.getLogger(GamePane.class.getName()).log(Level.SEVERE, null, ex);
                }};
        }
    }
    
    /**
     * Una inner class de un hilo que es el responsable de contabilizar el tiempo de esta parte del juego, en este caso los 60 segundos.  
     */
    private class HiloTiempo implements Runnable {

        @Override
        @SuppressWarnings("empty-statement")
        public void run() {
            while (inicializador) {
                try {
                    Thread.sleep(10);
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                    Logger.getLogger(GamePane.class.getName()).log(Level.SEVERE, null, ex);
                }
            };
            while (terminarJuego) {
                Platform.runLater(() -> {
                    tiempoTranscurrido -= 1;
                    if(tiempoTranscurrido>=10 && tiempoTranscurrido<60){    
                        tiempoLabel.setText(" 00:"+String.valueOf(tiempoTranscurrido));   
                    }else{
                        tiempoLabel.setText(" 00:0"+String.valueOf(tiempoTranscurrido));
                        Design.labelDesign(tiempoLabel, "2.5", "red");
                    }
                    if (tiempoTranscurrido == 0) {
                        terminarJuego = false;
                        sonido.stop();
                        rp= new ResultsPane();
                        TyperBallonsApp.getTheScene().setRoot(rp.getRoot());
                    }
                });
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                    Logger.getLogger(GamePane.class.getName()).log(Level.SEVERE, null, ex);
                }
            };
        }
    }
    
    /**
     * Una inner class de un hilo que es la cuenta regresiva para que comience el juego.
     */
    private class HiloInicio implements Runnable{
        @Override
        public void run() {
            for(int i=3;i>=1;i--){
                Platform.runLater(()->{
                   gamesPane.getChildren().clear(); 
                });
                Label l=new Label(String.valueOf(i));
                Design.labelDesign(l, "15");
                Platform.runLater(()->{
                 l.setLayoutX(CONSTANTES.GAME_WIDTH/2.5);
                l.setLayoutY(CONSTANTES.GAME_HEIGHT/3.5);
                gamesPane.getChildren().add(l);  
                });
                
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                    Logger.getLogger(GamePane.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            Platform.runLater(()->{
                gamesPane.getChildren().clear();
                gamesPane.getChildren().add(botonMenu());
            });
            inicializador=false;
            sonido= CargarRecurso.cargarSonido(CONSTANTES.PATH_AUDIO+"musicajuego1.wav");
            sonido.start();
        }
    }
    
    /**
     * Este método ejecuta un evento de tipo KeyEvent para que cada vez que el usuario presione una tecla, recorra los hijos del Pane(donde estarán los globos)
     * y verifique que dicho Code coincidan con las letras que posee cada globo.
     */
    private synchronized void BorrarLabel(){
        TyperBallonsApp.getTheScene().setOnKeyPressed((KeyEvent e)->{
            if(e.getText().matches("[A-Za-z]")){
            String code=e.getText(); ObservableList<Node> list=gamesPane.getChildren();
            list.stream().filter((n) -> (n instanceof Globo)).map((n) -> (Globo)n).forEachOrdered((globo) -> {
                String l=globo.getLabel().getText(); Clip boom= CargarRecurso.cargarSonido(CONSTANTES.PATH_AUDIO+"pinchazoglobo.wav");
                    if (gamesPane.getChildren().contains(globo) && l.contains(code)) {
                        if(globo instanceof GloboA || globo instanceof GloboG){
                            Platform.runLater(()->{
                                gamesPane.getChildren().remove(globo); boom.start(); cantidadGlobos+=1; cantidadLabel.setText(String.valueOf(cantidadGlobos));
                                agregarLetraGlobo(globo.getLabel().getText().toUpperCase());
                            });
                        }else if(globo instanceof GloboR){
                            GloboR gr=(GloboR)globo; String nuevo=l.replace(code,"");
                            if(nuevo.length()==0){
                                Platform.runLater(()->{
                                    gamesPane.getChildren().remove(globo);
                                    boom.start();
                                    cantidadGlobos+=1;
                                    cantidadLabel.setText(String.valueOf(cantidadGlobos));
                                }); String[] m=gr.getCopialabel().getText().split("");
                                for(String letraGR:m) agregarLetraGlobo(letraGR.toUpperCase());
                            }
                            gr.setLabelRojo(new Label(nuevo));
                        }
                    }
                });
            }
        });  
    }

        public Button botonMenu(){
        try {
            Button botonRegreso= new Button();
            Image img = new Image(new FileInputStream("src" + CONSTANTES.PATH_IMAGES + "iconoMenu.png"));
            ImageView imgV = new ImageView(img);
            imgV.setFitHeight(55);
            imgV.setFitWidth(65);
            botonRegreso.setGraphic(imgV);
            botonRegreso.setBackground(Background.EMPTY);   
            botonRegreso.setLayoutX(612);
            botonRegreso.setLayoutY(530);
            botonRegreso.setOnMouseClicked((e)->{
                sonido.stop();
                MenuPane mp= new MenuPane();
                TyperBallonsApp.getTheScene().setRoot(mp.getRoot());
                terminarJuego=false;
            });
            return botonRegreso;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GamePane.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}

