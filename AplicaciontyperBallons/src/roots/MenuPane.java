/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package roots;

import App.TyperBallonsApp;
import data.CONSTANTES;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import diseñonodos.Design;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author LissetteMedina
 */
public class MenuPane {

    private BorderPane menusPane;
    private StackPane opcionesPane;
    private GamePane gp;
    /**
     * Constructor que inicia la pantalla de Menú con sus respectivos nodos.
     */
    public MenuPane() {
        menusPane = new BorderPane();
        menusPane.setCenter(crearFondo());
        try {
            crearBotones();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MenuPane.class.getName()).log(Level.SEVERE, null, ex);
            
        }

    }
     /**
    * Método que setea el fondo de la pantalla Menú.
    * @return Devuelve un StackPane con el diseño editado.
    */
    public StackPane crearFondo() {
        //usamos como contenedor un Pane porque queremos colocar los elementos
        //de forma libre a los largo del Pane
        opcionesPane = new StackPane();
        opcionesPane.setPrefSize(CONSTANTES.GAME_WIDTH, CONSTANTES.GAME_HEIGHT);
        opcionesPane.setStyle("-fx-background-image: url('" + CONSTANTES.PATH_IMAGES + "fondo1.jpg');"
                + "-fx-background-repeat: stretch;"
                + "-fx-background-size: " + CONSTANTES.GAME_WIDTH + " " + CONSTANTES.GAME_HEIGHT + "; "
                + "-fx-background-position: center center;");
        return opcionesPane;
    }
    /**
     * Este método asocia un ícono al botón respectivo y modifica el diseño del
     * botón.
     * @param b, Botón al cual se le modificará el diseño. 
     * @param nImagen, Ícono asociado al botón. 
     * @throws FileNotFoundException Arroja error en caso de no encontrar el 
     * archivo de imagen.
     */
    public static void diseñoBoton(Button b, String nImagen) throws FileNotFoundException {
        Image img = new Image(new FileInputStream("src" + CONSTANTES.PATH_IMAGES + nImagen));
        ImageView imgV = new ImageView(img);
        imgV.setFitHeight(45);
        imgV.setFitWidth(50);
        b.setGraphic(imgV);
        Design.buttonDesign(b, "2.5");
    }
     /**
     * Este método crea botones en pantalla y se pasa el evento que le corres-
     * ponde a cada uno. 
     * Botón 1: Empieza el juego, Botón 2: muestra los 10 mejores Scores, 
     * Botón 3: Salir del juego.
     * @throws FileNotFoundException Arroja error en caso de no encontrar el 
     * archivo.
     */
    public void crearBotones() throws FileNotFoundException {

        VBox botones = new VBox();
        botones.setAlignment(Pos.CENTER);
        botones.setSpacing(10);
        Image titulo = new Image(new FileInputStream("src" + CONSTANTES.PATH_IMAGES + "titulojuego.png"));
        ImageView imgTitulo = new ImageView(titulo);
        imgTitulo.setFitHeight(200);
        imgTitulo.setFitWidth(450);
        Button b1 = new Button("INICIAR");
        diseñoBoton(b1, "iconoplay.png");
        Button b2 = new Button("VER PUNTAJE");
        diseñoBoton(b2, "iconopuntaje.png");
        Button b3 = new Button("SALIR");
        diseñoBoton(b3, "iconosalir.png");
        botones.getChildren().addAll(imgTitulo, b1, b2, b3);
        opcionesPane.getChildren().add(botones);
        menusPane.setCenter(opcionesPane);

        b1.setOnMouseClicked((e) -> {
            gp = new GamePane();
            TyperBallonsApp.getTheScene().setRoot(gp.getRoot());
        });
        b2.setOnMouseClicked((e) -> {
            ScorePane2 rp = new ScorePane2();
            TyperBallonsApp.getTheScene().setRoot(rp.getRoot());
        });
        b3.setOnMouseClicked((e) -> {
            Platform.exit();
        });
    }

    public BorderPane getRoot() {
        return menusPane;
    }

    public void setGp(GamePane gp) {
        this.gp = gp;
    }

    public GamePane getGp() {
        return gp;
    }
}
