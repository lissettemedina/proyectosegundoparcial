/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

/**
 *
 * @author LissetteMedina
 */
public class CONSTANTES {

    private CONSTANTES() {
    }
    
    public static final String PATH_FILES = "src/recursos/";
    public static final String PATH_IMAGES = "/recursos/";
    public static final double GAME_WIDTH = 700;
    public static final double GAME_HEIGHT = 700;
    public static final String[] TIPO_GLOBOS = {"v", "a", "r"};
    public static final String PATH_SERIALIZABLE = "src/scores/";
    public static final String PATH_AUDIO= "/recursos/";
    public static final String ESTILO= "-fx-font-family: \"Eras Bold ITC\",";
    public static final String tamano= "-fx-font-size:";
    public static final String letra="Georgia, Sans Serif;";
    
    
}
