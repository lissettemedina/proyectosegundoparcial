/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import Exceptions.ArchivoExcepcion;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author LissetteMedina
 */
public class DiccionarioData {

    private DiccionarioData() {
    }
    
    

    static String ruta = CONSTANTES.PATH_FILES + "espanol.csv";

    /**
     * Genera un mapa donde sus claves son letras sin tilde, y su valor la misma letra pero con tilde.
     * @param v,Letra del abecedario.
     * @return Devuelve la letra pasado como parámetro pero con tilde.
     */
    static public String tildeVocal(String v) {
        Map<String,String> vocal = new  HashMap<>();
        vocal.put("a","á");
        vocal.put("e","é");
        vocal.put("i","í");
        vocal.put("o","ó");
        vocal.put("u","ú");
        if (vocal.containsKey(v.toLowerCase())) {
            return vocal.get(v.toLowerCase());
        }
        return null;
    }
    /**
     * Genera un mapa donde sus claves son letras con tilde, y su valor la misma letra pero sin tilde.
     * @param v, Letra del abecedario con tilde.
     * @return Devuelve la letra pasada como parámetro pero sin tilde.
     */
 static public String sinTildeVocal(String v) {
        Map<String,String> vocal = new  HashMap<>();
        vocal.put("á","a");
        vocal.put("é","e");
        vocal.put("í","i");
        vocal.put("ó","o");
        vocal.put("ú","u");
        if (vocal.containsKey(v.toLowerCase())) {
            return vocal.get(v.toLowerCase());
        }
        return null;
    }
    
 /**
  * Este método abre el archivo espanol.csv y valida las palabras que comiencen con la letra pasada como parámetro. 
  * @param letra,Letra del abecedario. 
  * @return Devuelve un List donde sus elementos son las palabras que comienzas con la letra pasada como parámetro pero sin tilde y minúscula.
  * @throws ArchivoExcepcion  Arroja un error si no se ha encontrado el archivo.
  */
    public static List<String> cargarPalabras(String letra) throws ArchivoExcepcion {
        String letraT = tildeVocal(letra); 
        List<String> palabras = new ArrayList<>();
        try (BufferedReader bf = new BufferedReader(new FileReader(ruta))) { 
            String linea;
            while ((linea = bf.readLine()) != null) { 
                String[] p = linea.split("\n");
                for (String a : p) {
                    if (!a.toLowerCase().equals(letra.toLowerCase())) {
                        if(letraT!=null){
                            if (a.toLowerCase().trim().startsWith(letra.toLowerCase()) || a.toLowerCase().trim().startsWith(letraT.toLowerCase())) palabras.add(quitarTildePalabra(a));
                        }
                        else if (a.toLowerCase().trim().startsWith(letra.toLowerCase())) palabras.add(quitarTildePalabra(a));
            }}}}catch (IOException e) {throw new ArchivoExcepcion(ruta, e.getMessage());}
        return palabras;}
    
    /**
     * Este método quita la tilde de una palabra pasada como parámetro.
     * @param palabra1, String que es una palabra.
     * @return Devuelve la palabra sin tilde.
     */
    public static String quitarTildePalabra(String palabra1){
        String nuevaPalabra="";
        String palabra= palabra1.toLowerCase();
        for( int i=0; i<palabra.length();i++){
            if(sinTildeVocal(String.valueOf(palabra.charAt(i)))==null)
                nuevaPalabra+=String.valueOf(palabra.charAt(i));  
            else
                nuevaPalabra+= sinTildeVocal(String.valueOf(palabra.charAt(i)));
        }
        return nuevaPalabra.toLowerCase();
    }
    
    /**
     * Este método valida si existe una palabra pasada como parámetro en la lista del método "cargarPalabras()".
     * @param pal,String que es una palabra
     * @return Devuelve un booleano si dicha palabra existe en un la lista "cargarPalabras()"
     */
    public static boolean validarPalabra(String pal){
        boolean bandera = false;
        try {
            if (pal != null && !pal.equals("")){
                String palabra = quitarTildePalabra(pal);
                if (cargarPalabras(String.valueOf(palabra.toLowerCase().charAt(0))).contains(palabra.toLowerCase())){
                    bandera = true;
                }
            }
            
        } catch (ArchivoExcepcion | StringIndexOutOfBoundsException ex) {
            Logger.getLogger(DiccionarioData.class.getName()).log(Level.SEVERE, null, ex);
        }
        return bandera;
    }    
}
