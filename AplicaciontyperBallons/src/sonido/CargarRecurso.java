/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sonido;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 *
 * @author LissetteMedina
 */
public class CargarRecurso {

    private CargarRecurso() {
    }
    
    
    public static Clip cargarSonido(final String ruta){
        Clip clip= null;
        try{
            InputStream is= ClassLoader.class.getResourceAsStream(ruta);
            AudioInputStream ais= AudioSystem.getAudioInputStream(new BufferedInputStream(is));
            DataLine.Info info= new DataLine.Info(Clip.class, ais.getFormat());
            clip = (Clip)AudioSystem.getLine(info);
            clip.open(ais);
        }catch (IOException | LineUnavailableException | UnsupportedAudioFileException e){
            Logger.getLogger(CargarRecurso.class.getName()).log(Level.SEVERE, null, e);
        }
        return clip;
    }
}
