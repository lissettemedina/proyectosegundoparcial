/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import data.CONSTANTES;
import java.util.Random;
import javafx.scene.control.Label;

/**
 *
 * @author PedroFarinango
 */
public final class GloboR extends Globo {
    private Label copialabel;
    
    public GloboR() {
        super(CONSTANTES.PATH_IMAGES + "globoR.png");
        labelRojo();
    }
    
    /**
     * Se setea el label del globo rojo que son letras minúsculas con una cantidad de 2 o 3 letras.
     */
    public void labelRojo() {
        Random r = new Random();
        Label l = new Label();
        l.setStyle("-fx-font-family: \"Eras Bold ITC\", "
                + "Georgia, Sans Serif;" + "-fx-font-size: 2.3em;" + "-fx-text-fill:#ffffff");
        if (r.nextInt(2) == 0) {
            l.setText(Palabra.generarDosLetrasAleatorias().toLowerCase());
            this.copialabel=l;
            super.setLabel(l);
        } else {
            l.setText(Palabra.generarTresLetrasAleatorias().toLowerCase());
            this.copialabel=l;
            super.setLabel(l);
        }
    }
    
    public void setLabelRojo(Label l){
        l.setStyle("-fx-font-family: \"Eras Bold ITC\", "
                + "Georgia, Sans Serif;" + "-fx-font-size: 2.3em;" + "-fx-text-fill:#ffffff");
        super.setLabel(l);
    }

    public Label getCopialabel() {
        return copialabel;
    }
    
}
