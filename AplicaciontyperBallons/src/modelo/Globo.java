/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;

/**
 *
 * @author PedroFarinango
 */
public abstract class Globo extends StackPane {

    String ruta;
    private double frecuencia;
    private ImageView imgv;
    protected Label label;

    /**
     * En este constructor se crea la imagen de un globo, y se lo agrega al mismo objeto ya que es un StacPane.
     * @param ruta, La ruta donde está la imagen de un globo. 
     */
    public Globo(String ruta){
        this.ruta = ruta;
        //Frecuencia con la que sube el globo
        this.frecuencia = 3;
        Image img = new Image(getClass().getResourceAsStream(ruta),
                190,
                220,
                true,
                true);
        imgv = new ImageView(img);
        label = new Label();
        this.getChildren().addAll(imgv, label);
    }

    public ImageView getImgv() {
        return imgv;
    }

    public void setImgv(ImageView imgv) {
        this.imgv = imgv;
    }

    public Label getLabel() {
        return label;
    }

    public void setLabel(Label label) {
        this.getChildren().remove(this.label);
        this.label=label;
        StackPane.setAlignment(this.label, Pos.TOP_CENTER);
        this.getChildren().add(label);
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public double getFrecuencia() {
        return frecuencia;
    }

    public void setFrecuencia(double frecuencia) {
        this.frecuencia = frecuencia;
    }

    public void fijarPosicion(double x, double y) {
        this.setLayoutX(x);
        this.setLayoutY(y);
    }

    public void cambiarPosicionY() {
        this.setLayoutY(this.getLayoutY() - 1);
    }
}
