/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 *
 * @author LissetteMedina
 */
public abstract class Palabra {

    /*Declaramos el abecedario como estático, ya que es un dato que pertenece 
    como tal a la clase.*/
    public final static String[] abecedario = {"a", "b", "c", "d", "e", "f", "g", "h",
        "i", "j", "k", "l", "m", "n", "ñ", "o", "p", "q", "r", "s", "t", "u", "v",
        "w", "x", "y", "z"};
    protected final static int[] puntaje = {1, 3, 3, 2, 1, 4, 2, 4, 1, 8, 5, 1, 3, 1, 8, 1, 3, 5, 1, 1, 1, 1, 4, 5, 8, 4, 10};
    
    protected final static String[] abecedario2 = {"a","a", "b","c", "d","e","e", "f", "g", "h",
        "i","i", "j", "k", "l", "m", "n", "ñ", "o","o", "p", "q", "r", "s", "t", "u","u", "v",
        "w", "x", "y", "z"};
    
    
    
    public String[] getAbecedario(){
        return abecedario;
    }
    public int[] getPuntaje(){
        return puntaje;
    }

    /**
     * Esta funcion recorre la lista de abecedario para asociarla a los
     * elementos del puntaje mediante su índice y posteriormente agregarlos en
     * una collección HashMap FORMATO HASHMAP
     *
     * @param letra,letra del abecedario
     * @return int
     */
    public static int letraPuntaje(String letra) {
        Map<String, Integer> mapa = new HashMap<>();
        for (int i = 0; i < abecedario.length; i++) {
            mapa.put(abecedario[i], puntaje[i]);
        }
        for (Map.Entry<String, Integer> obj : mapa.entrySet()) {
            String clave=obj.getKey();
            if (letra.toLowerCase().trim().equals(clave))return mapa.get(clave);
        }
        return 0;
        }
       
    

    public Palabra() {
    }

    /**
     * Genera una palabra aleatoria
     * @return Devuelve un String con longitud 1.
     */
    public static String generarLetraAleatoria() {
        Random r = new Random();
        return abecedario2[r.nextInt(abecedario2.length)];
    }

    /**
     * Genera dos palabras aleatorias
     * @return Devuelve un String con longitud 2.
     */
    public static String generarDosLetrasAleatorias() {
        return generarLetraAleatoria() + generarLetraAleatoria();
    }

    /**
     * Genera tres palabras aleatorias.
     * @return Devuelve un String con longitud3.
     */
    public static String generarTresLetrasAleatorias() {
        return generarLetraAleatoria() + generarLetraAleatoria()
                + generarLetraAleatoria();
    }
    
    /**
     * Este metodo valida si es un string con letras del abecedario(sin tomar en cuenta los espacios).
     * @param letra,Una letra del abecedario.
     * @return Devuelve un booleano.
     */
    public static boolean validarStringVacio(String letra){
        String r=letra.toLowerCase().trim();
        for(int i=0;i<letra.length();i++){
            String charr =Character.toString(r.charAt(i));
            if(!charr.matches("[a-z]")){
                return false;
            }
        }
        return true;
    }
}
