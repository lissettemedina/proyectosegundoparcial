/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import data.CONSTANTES;
import javafx.scene.control.Label;

/**
 *
 * @author PedroFarinango
 */
public final class GloboG extends Globo {

    public GloboG() {
        super(CONSTANTES.PATH_IMAGES + "globoV.png");
        labelG();
    }
    
    /**
     * Se setea el label del globo verde que es una letra minúscula.
     */
    public void labelG() {
        Label l = new Label(Palabra.generarLetraAleatoria().toLowerCase());
        l.setStyle("-fx-font-family: \"Eras Bold ITC\", "
                + "Georgia, Sans Serif;" + "-fx-font-size: 2.7em;" + "-fx-text-fill:#ffffff");
        super.setLabel(l);
    }
}
