/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import data.CONSTANTES;
import javafx.scene.control.Label;

/**
 *
 * @author PedroFarinango
 */
public final class GloboA extends Globo {
    public GloboA(){
        super(CONSTANTES.PATH_IMAGES + "globoA.png");
        labelA();
    }
    
    /**
     * Se setea el label del globo amarillo que es una letra mayúscula.
     */
     public void labelA() {
        Label l = new Label(Palabra.generarLetraAleatoria().toUpperCase());
        l.setStyle("-fx-font-family: \"Eras Bold ITC\", "
                + "Georgia, Sans Serif;" + "-fx-font-size: 3.5em;" + "-fx-text-fill:#ffffff");
        super.setLabel(l);
     }
}
