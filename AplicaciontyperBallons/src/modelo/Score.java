/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import data.CONSTANTES;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author LissetteMedina
 */
public class Score implements Serializable, Comparable<Score> {
   private final int puntaje;
   private final String nombre;
   private static final Path p=Paths.get(CONSTANTES.PATH_SERIALIZABLE+"score.dat");
    /**
     * Este constructor se forma para almacenar la información respectiva de un 
     * jugador y almacenarla posteriormente en un Set.
     * @param puntaje, score final alcanzado por el usuario en el juego.
     * @param nombre, nombre del jugador. 
     */
    public Score(int puntaje, String nombre) {
        this.puntaje = puntaje;
        this.nombre = nombre;
    }

    public int getPuntaje() {
        return puntaje;
    }

    public String getNombre() {
        return nombre;
    }
    
    /**
     * Este método realiza la serialización de una colección que tiene como 
     * elementos objetos de tipo Score.
     * @throws IOException Arroja un error si no se ha encontrado el archivo.
     */
    public void escribirArchivo() throws IOException{
        try(ObjectOutputStream objectOutputStream= new ObjectOutputStream(new FileOutputStream(p.toFile()))){
            TreeSet <Score> finalScore= new TreeSet<>();
            finalScore.add(this);
            objectOutputStream.writeObject(finalScore);
        } 
    }
    /**
     * Este metodo realiza la serialización de una colección que tiene como 
     * elementos objetos de tipo Score.
     * @param s, Colección donde se encuentra almacenado los scores de los juga-
     * dores.
     */
    public void escribirArchivo(TreeSet<Score> s){
        try(ObjectOutputStream objectOutputStream= new ObjectOutputStream(new FileOutputStream(p.toFile()))){
            s.add(this);
            objectOutputStream.writeObject(s);
       } catch (IOException ex){
            Logger.getLogger(Score.class.getName()).log(Level.SEVERE, null, ex);
       }
    }
    /**
     * Este método deserializa una colección Set que tiene elementos tipo Score.
     * @return Devuelve el Set deserializado. 
     * @throws FileNotFoundException Arroja este error cuando el archivo no se
     * encuentra.
     * @throws IOException Arroja este error por cualquier incoveniente con el 
     * archivo.
     * @throws ClassNotFoundException Puede ser lanzada por el método readObject
     * al instante de deserializar el objeto si no se encuentra la clase con la
     * cual deserializarla.
     */
    public static TreeSet<Score> leerArchivo() throws FileNotFoundException,IOException,ClassNotFoundException {

      try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(p.toFile()))){
          Object obj=objectInputStream.readObject();
          TreeSet<Score> finalScore= (TreeSet<Score>)obj;
          return finalScore;
    }
    }
    /**
     * Este método compara los elementos en el TreeSet por su Puntaje y luego por
     * su nombre.
     * @param s, Objeto Score.
     * @return Devuelve un valor entero positivo, negativo o cero de acuerdo
     * a las comparaciones hechas por el método compareTo.
     */
    @Override
    public int compareTo(Score s) {
        Integer punt=puntaje;
        if (punt.compareTo(s.puntaje)==0){
            return nombre.compareTo(s.nombre);    
        }
            return punt.compareTo(s.puntaje)*-1;
    }
}
